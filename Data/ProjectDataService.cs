namespace jt0dd.xyz.Data;
using System;
using System.Data;
using System.Text.Json;

public class ProjectDataService
{
    private const string filename = "ProjectData.json";
    
    public Task<ProjectData> GetProjectData() {
        var Ret = new List<string>();

        string jsonString = File.ReadAllText(filename);
        ProjectData data = JsonSerializer.Deserialize<ProjectData>(jsonString)!;

        return Task.FromResult(data);
    }

    public Task<int> SetProjectData(string name, string mediaLoc, string desc, bool image, string link) {
        var options = new JsonSerializerOptions { WriteIndented = true };

        ProjectData data = new ProjectData();

        data.projectName = name;
        data.projectMediaLocation = mediaLoc;
        data.projectDesc = desc;
        data.projectImage = image;
        data.projectLink = link;

        string jsonString = JsonSerializer.Serialize(data, options);
        File.WriteAllText(filename, jsonString);

        return Task.FromResult(0);
    }
}