namespace jt0dd.xyz.Data;

public class ProjectData
{
    public string projectName { get; set; } = "";
    public string projectMediaLocation { get; set; } = "";
    public string projectDesc { get; set; } = "";
    public bool projectImage { get; set; } = false;
    public string projectLink { get; set; } = "";
}